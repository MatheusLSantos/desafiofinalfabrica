import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.Keys;

import java.util.List;

public class DesafioFabrica {
    private WebDriver driver;
    @Before
    public void abrir(){
        //O que fazer antes do teste
        System.setProperty("webdriver.gecko.driver", "C:\\Windows\\geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://www.nagem.com.br/");
        Assert.assertEquals("Nagem - Fique à vontade, fique Nagem", driver.getTitle());
    }
    @After
    public void sair(){
        //O que fazer depois do teste
        driver.quit();
    }
    @Test
    public void deveAdicionarNoCarrinho()throws InterruptedException {
        //Produto que será pesquisado
        String termoDeBusca = "Xbox";
        Thread.sleep(3000);

        //Encontrar a barra de pesquisa e digitar o produto
        WebElement caixaDePesquisa = driver.findElement(By.xpath("//*[@id=\"buscaGeral\"]"));
        caixaDePesquisa.sendKeys(termoDeBusca);

        //Clicar no botão de pesquisa
        WebElement botaoPesquisa = driver.findElement(By.xpath("//*[@id=\"header-campoBusca\"]/div/form/div/span/button"));
        botaoPesquisa.click();

        //Atraso proposital para carregamento dos componentes
        Thread.sleep(3000);

        //Verificar se o resultado da pesquisa condiz com o que foi pedido
        WebElement resultadoPesquisa = driver.findElement(By.xpath("//*[@id=\"prodList_1\"]/div/div[2]/div[1]/h3"));
        String nomeProduto = resultadoPesquisa.getText();
        nomeProduto = nomeProduto.toLowerCase();
        termoDeBusca = termoDeBusca.toLowerCase();
        boolean resultadoTeste;
        if (nomeProduto.contains(termoDeBusca)){
            System.out.println("Busca encontrou: "+nomeProduto);
            System.out.println("Validação OK!");
            resultadoTeste = true;
        }else {
            System.out.println("Erro, termo não encontrado: "+termoDeBusca);
            System.out.println("Resultado encontrado: "+nomeProduto);
            resultadoTeste = false;
        }
        //Validando se o resultado é verdadeiro
        Assert.assertTrue(resultadoTeste);

        //Clicar no produto
        Thread.sleep(1000);
        resultadoPesquisa.click();

        //Aceitar cookies
        WebElement aceitarCookie = driver.findElement(By.xpath("//*[@id=\"oPrivallyApp-AcceptLink\"]"));
        aceitarCookie.click();
        Thread.sleep(2000);

        //Clicar para comprar / Adicionar no carrinho
        WebElement botaoComprar = driver.findElement(By.id("btnComprar"));
        botaoComprar.click();
        Thread.sleep(3000);

        //Confirmar escolha do produto
        WebElement confirmarEscolha = driver.findElement(By.xpath("//*[@id=\"btn-continue\"]"));
        confirmarEscolha.click();
        Thread.sleep(3000);

        //Validar carrinho
        WebElement abaCarrinho = driver.findElement(By.xpath("/html/body/div[2]/div[1]/div[2]/div/div[1]/div/h2"));
        String carrinho = abaCarrinho.getText();
        Assert.assertEquals("Seu Carrinho", carrinho);
        System.out.println("Validação Aba carrinho OK");

        //Validar produto no carrinho
        WebElement produtoCarrinho = driver.findElement(By.xpath("//*[@id=\"linhaProduto_525316\"]/div[1]/div/div[2]/p/a"));
        String nomeProdutoCarrinho = produtoCarrinho.getText();
        nomeProdutoCarrinho = nomeProdutoCarrinho.toLowerCase();
        boolean validarProdutoCarrinho;
        if (nomeProdutoCarrinho.contains(nomeProduto)){
            System.out.println("Produto no carrinho: "+nomeProdutoCarrinho);
            System.out.println("Validação OK!");
            validarProdutoCarrinho = true;
        }else {
            System.out.println("Erro, produto nao encontrado no carrinho: "+nomeProduto);
            System.out.println("Resultado encontrado: "+nomeProdutoCarrinho);
            validarProdutoCarrinho = false;
        }
        //Validando se o resultado é verdadeiro
        Assert.assertTrue(validarProdutoCarrinho);

    }




}
